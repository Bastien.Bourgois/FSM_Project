require 'state_machine'
require 'highline/import'

class PopVehicle
  state_machine :initial => :parked do

    event :park do
      transition [:indling] => :parked
    end

    event :ignite do
      transition [:parked] => :indling
    end
    event :gear_up do
      transition [:indling] => :first_gear,
                  [:fisrt_gear] => :second_gear,
                  [:second_gear] => :third_gear,
                  [:third_gear] => :crashed
    end
    event :gear_down do
      transition [:first_gear] => :indling,
                  [:second_gear] => :first_gear,
                  [:third_gear] => :second_gear
    end
    event :crash do
      transition [:first_gear, :second_gear, :third_gear] => :crashed
    end
    event :repair do
      transition [:crashed] => :parked
    end
  end
end

vehicle = PopVehicle.new

while true do
  puts "Le véhicule est #{vehicle.state}"
  puts "et peut recevoir #{vehicle.state_events}"
  input = ask "-> Action ?: "
  vehicle.send(input)
end
